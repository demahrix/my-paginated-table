import 'dart:math' show min;
import 'package:flutter/material.dart';

class MyPaginatedDataTable<T> extends StatefulWidget {

  final double dataRowHeight;
  final DataRow Function(T) rowBuilder;

  final List<DataColumn> columns;
  final Future<List<T>> Function(int page, int count, T? lastItem) onPageChanged;

  /// Permet de calculer le nombre
  final int? allRowsCount;

  final Widget loadingWidget;

  final Widget Function(dynamic err)? errorBuilder;

  final bool showCheckboxColumn;

  const MyPaginatedDataTable({
    Key? key,
    this.dataRowHeight = kMinInteractiveDimension,
    required this.rowBuilder,
    required this.onPageChanged,
    this.allRowsCount,
    required this.columns,
    this.loadingWidget = const Center(child: CircularProgressIndicator()),
    this.errorBuilder,
    this.showCheckboxColumn = false
  }): super(key: key);

  @override
  _PaginatedDataTableState<T> createState() => _PaginatedDataTableState<T>();

}

class _PaginatedDataTableState<T> extends State<MyPaginatedDataTable<T>> {

  int? _countPerRow; // count of row
  int _currentPage = 0;
  int? _maxPage;

  Future<List<T>>? _future;

  static const TextStyle _paginatedControlsTextStyle = TextStyle(
    fontWeight: FontWeight.w500
  );

  void _initialFetch() {
    _future = widget.onPageChanged(_currentPage, _countPerRow! - 1, null);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {});
    });
  }

  void _fetchData(bool next, T? lastItem) {
    setState(() {
      _future = widget.onPageChanged(_currentPage, _countPerRow! - 1, lastItem); // -1 car le dernier element est le controlleur de pagination
    });

    _future!.then((value) => {
      // increment _currentPage ou decrement
      setState(() {
        if (next) {
          ++_currentPage;
        }
        else {
          --_currentPage;
        }
      })
    });
  }

  /// true: suivant, false: precedent
  void _onPageChanged(bool next, T lastItem) {
    _fetchData(next, lastItem);
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (_, contraints) {

        final double maxHeight = contraints.maxHeight;

        _countPerRow ??= maxHeight ~/ widget.dataRowHeight;

        if (_maxPage == null && widget.allRowsCount != null)
          _maxPage = (widget.allRowsCount! / _countPerRow!).ceil();

        if (_future == null) // on charge les donnees la premiere fois lorsque on a le nombre d'elements par page
          _initialFetch();

        return SizedBox(
          height: maxHeight,
          child: FutureBuilder<List<T>>(
            future: _future,
            builder: (_, snapshot) {
              return Column(
                children: [
                  _tableBuilder(snapshot),
                  _paginatedControls(
                    snapshot.data
                  )
                ],
              );
            }
          ),
        );
      }
    );
  }

  Widget _tableBuilder(AsyncSnapshot<List<T>> snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {

      if (snapshot.hasError)
        return widget.errorBuilder?.call(snapshot.error)
          ?? _defaultErrorBuilder(null);

      final data = snapshot.data!;

      final int length = min(_countPerRow! - 1, data.length);

      return SizedBox(
        width: double.infinity,
        child: DataTable(
          dataRowHeight: widget.dataRowHeight,
          columns: widget.columns,
          showCheckboxColumn: widget.showCheckboxColumn,
          showBottomBorder: true,
          rows: List.generate(length, (index) => widget.rowBuilder(data[index])),
        ),
      );
    }

    return widget.loadingWidget;
  }

  Widget _paginatedControls(List<T>? data) {

    // Si data == null est en chargement ou il y a eu une erreur

    int offset = _currentPage * _countPerRow! + 1;
    int? count = data?.length;


    bool backButtonDisabled = data == null
      || _currentPage == 0;

    bool nextButtonDisabled = data == null
      || _currentPage == _maxPage
      || count! < _countPerRow!; // si data != null -> count != null

    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [

        Text(
          offset.toString(),
          style: _paginatedControlsTextStyle,
        ),

        const Text('-', style: _paginatedControlsTextStyle),

        Text(
          count == null
            ? ' '
            : (offset + 1).toString(),
          style: _paginatedControlsTextStyle,
        ),

        if (_maxPage != null)
          Text(
            ' sur $_maxPage',
            style: _paginatedControlsTextStyle,
          ),

        const SizedBox(width: 30.0,),

        IconButton(
          splashRadius: 18.0,
          onPressed: backButtonDisabled
            ? null
            : () => _onPageChanged(false, data.last),
          icon: const Icon(Icons.arrow_back_ios_rounded, size: 15.0,),
        ),

        // on desactive le button suivant si on atteint la dernier page
        // Ou si le nombre d'element est 
        IconButton(
          onPressed: nextButtonDisabled
            ? null
            : () => _onPageChanged(true, data.last),
          icon: const Icon(Icons.arrow_forward_ios_rounded, size: 15.0,)
        ),

        const SizedBox(width: 15.0)
      ],
    );
  }

  Widget _defaultErrorBuilder(_) {
    return const Center(
      child: Text("Une erreur s'est produite"),
    );
  }

}
